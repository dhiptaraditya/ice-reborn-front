# Ice Cream Festival

Developed by:
**C3 Team**
- Annida Safira Arief (1706040050)
- Ardian Jati Permadi (1506689042)
- Dhipta Raditya Yudhasmara (1706028644)
- Razaqa Dhafin Haffiyan (1706039484)
- Siti Aulia Rahmatussyifa (1706022073)

## Deskripsi
Ice Cream Festival (ICF) adalah sebuah aplikasi vending machine yang dapat diakses oleh penjual dan pembeli dimana si pembeli dapat memilih dan membayar es-krim dan topping yang ada. Karena menggunakan e-money, pembeli juga dapat melakukan top-up. Sedangkan, penjual dapat melakukan apa yang pembeli lakukan dan ditambah restock es-krim.

## Fitur
- Login dan pemilihan role (**untuk pembeli dan pemilik**)
    >Pada fitur ini, user akan memilih role mereka sebelum mengakses vending machine. User dapat memilih role sebagai pembeli ataupun pemilik. Apabila memilih sebagai pembeli, maka user dapat melakukan pembelian dan top-up pada vending machine (fitur b, c, e). Sedangkan jika menjadi pemilik, maka user dapat melakukan semua yang dapat dilakukan pembeli ditambah akses ke restock barang (fitur d).
- Pemilihan es-krim dan juga topping toppingnya (**untuk pembeli dan pemilik**)
    >Setelah login, user dapat melihat tampilan menu beserta harganya dan memilih es-krim dan topping-nya. Pada vending machine, akan disediakan berbagai macam pilihan base es-krim dan topping. User harus dan hanya memilih satu jenis es-krim. Sedangkan untuk topping, user dapat menambah topping sebanyak-banyaknya atau tidak memakai topping.
- Pembayaran atas barang yang telah dipilih(untuk pembeli dan pemilik)
    >Pada fitur ini, vending machine akan mengakumulasi harga dari es-krim dan semua topping yang dipilih. Pembeli menggunakan uang yang ada pada akunnya untuk membayar es-krim beserta toppingnya. Apabila sudah terbeli, otomatis saldo di e-money pembeli akan berkurang, sedangkan saldo di e-money penjual akan bertambah.
- Melakukan re-stock barang (untuk pemilik)
    >Karena jumlah es-krim dan topping di vending machine akan terus berkurang saat dibeli, maka fitur ini memberikan keleluasaan bagi penjual untuk menambahkan stok es-krim dan topping yang disediakan. Terutama apabila stok barang sudah habis, maka pembeli tidak dapat membeli kecuali sudah direstock oleh pemilik.
- Top up e-money(untuk pembeli dan pemilik)
    >Pada fitur ini, pembeli dapat melakukan top-up untuk mengisi ulang saldo pada e-money mereka terutama saat saldo pada e-money telah habis. Isi ulang tersebut dilakukan di vending machine ini dan harus dilakukan karena saldo pembeli akan berkurang sampai habis jika terus membeli, sedangkan pemilik akan bertambah saldonya.
