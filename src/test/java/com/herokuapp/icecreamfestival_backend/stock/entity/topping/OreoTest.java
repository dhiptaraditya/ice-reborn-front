package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.base.Banana;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OreoTest {
    private Oreo bananaOreo;

    @Before
    public void setUp() {
        bananaOreo = new Oreo(new Banana());
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.9, bananaOreo.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Banana with extra, Oreo", bananaOreo.getDescription());
    }
}
