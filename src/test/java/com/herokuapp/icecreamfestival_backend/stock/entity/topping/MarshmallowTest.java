package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.base.Chocolate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarshmallowTest {
    private Marshmallow rockyroad;

    @Before
    public void setUp() {
        rockyroad = new Marshmallow(new Chocolate());
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.5, rockyroad.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Chocolate with extra, Marshmallow ", rockyroad.getDescription());
    }
}
