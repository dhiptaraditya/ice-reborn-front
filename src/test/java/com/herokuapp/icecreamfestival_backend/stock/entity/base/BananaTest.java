package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BananaTest {
    private Banana banana;

    @Before
    public void setUp() {
        banana = new Banana();
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.5, banana.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Banana", banana.getDescription());
    }

}