package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.base.Banana;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HoneyStarTest {
    private HoneyStar bananaStar;

    @Before
    public void setUp() {
        bananaStar = new HoneyStar(new Banana());
    }

    @Test
    public void testMethodCost() {
        assertEquals(3.0, bananaStar.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Banana with extra, Honey Star", bananaStar.getDescription());
    }
}
