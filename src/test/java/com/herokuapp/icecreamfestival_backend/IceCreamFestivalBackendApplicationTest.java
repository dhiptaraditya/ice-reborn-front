/*
package com.herokuapp.icecreamfestival_backend;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CheckoutController.class)
public class IceCreamFestivalBackendApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testTitle2() throws Exception {
        HashMap<String, String> buyItem = new HashMap<String, String>();
        buyItem.put("user","dhafin");
        buyItem.put("base_layer","Chocolate");
        buyItem.put("Oreo","2");
        buyItem.put("Marshmallow","0");
        buyItem.put("Popcorn","1");
        buyItem.put("Chocochip","1");
        buyItem.put("HoneyStar","1");

        this.mockMvc.perform(post("/api/checkout")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(new JSONObject(buyItem))))
                .andExpect(status().isOk());
    }


    @Test
    public void applicationRunTest() {
        IceCreamFestivalBackendApplication.main(new String[]{
                "--spring.main.web-environment=false",
        });
    }


}
*/