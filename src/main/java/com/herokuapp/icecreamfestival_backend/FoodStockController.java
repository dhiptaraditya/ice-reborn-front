package com.herokuapp.icecreamfestival_backend;

import com.herokuapp.icecreamfestival_backend.requests.RestockRequest;
import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import com.herokuapp.icecreamfestival_backend.stock.repository.FoodStockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.*;

@Controller
public class FoodStockController {

    @Autowired
    private FoodStockRepository foodStockRepository;

    @GetMapping("/greeting")
    public String greeting(OAuth2Authentication authentication, Model model) {
        LinkedHashMap<String, String> details = (LinkedHashMap<String, String>) authentication.getUserAuthentication().getDetails();
        model.addAttribute("name", details.get("name"));
        return "greeting";
    }

    @PostMapping("/link")
    public ResponseEntity<?> restock(@RequestBody RestockRequest request) {
        String[] daftar_nama_stock = {"Banana", "Chocolate", "SeaSalt", "Chocochip", "HoneyStar", "Marshmallow", "Oreo", "Popcorn"};
        FoodStock stock = foodStockRepository.findByName(request.getItemName());
        if (stock == null) {
            HashMap<String, Object> response = new HashMap<>();
            response.put("message", "Tidak ada nama makanan" + request.getItemName());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(response);
        }
        stock.setStock(stock.getStock() + request.getStok());
        foodStockRepository.save(stock);
        HashMap<String, Object> response = new HashMap<>();
        response.put("name", stock.getName());
        response.put("stock", stock.getStock());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(response);
    }


    @RequestMapping(value = "/api/stocks/all", method = RequestMethod.GET)
    public ResponseEntity<?> get_stock_all() {
        String[] daftar_nama_stock = {"Banana", "Chocolate", "SeaSalt", "Chocochip", "HoneyStar", "Marshmallow", "Oreo", "Popcorn"};
        HashMap<String, Object> response = new HashMap<>();
        ArrayList<FoodStock> daftar_stock = new ArrayList<FoodStock>();
        for (String name : daftar_nama_stock) {
            FoodStock stock = foodStockRepository.findByName(name);
            daftar_stock.add(stock);
        }
        response.put("data", daftar_stock);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @RequestMapping(value = "/api/restocks", method = RequestMethod.POST)
    public ResponseEntity<?> restock_many(@RequestBody Map<String, Integer> restock_amount) {
        Map<String, Integer> map = restock_amount;
        HashMap<String, Object> response = new HashMap<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            FoodStock stock = foodStockRepository.findByName(entry.getKey());
            stock.setStock(stock.getStock() + entry.getValue());
            foodStockRepository.save(stock);
            response.put(entry.getKey(), foodStockRepository.findByName(entry.getKey()).getStock());
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @RequestMapping(value = "/api/fillstock", method = RequestMethod.POST)
    public ResponseEntity<?> fill() {
        String[] daftar_nama_stock = {"Banana", "Chocolate", "SeaSalt", "Chocochip", "HoneyStar", "Marshmallow", "Oreo", "Popcorn"};
        Integer[] harga_barang = {1, 2, 3, 4, 5, 6, 7, 8};
        HashMap<String, Object> response = new HashMap<>();
        for (int i = 0; i < daftar_nama_stock.length; i++) {
            if (foodStockRepository.findByName(daftar_nama_stock[i]) == null) {
                FoodStock stock = new FoodStock(daftar_nama_stock[i], harga_barang[i]);
                foodStockRepository.save(stock);
                response.put(stock.getName(), stock.getStock());
            }
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }


}
