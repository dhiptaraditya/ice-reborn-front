package com.herokuapp.icecreamfestival_backend;

import java.security.Principal;
import java.util.Arrays;

import com.herokuapp.icecreamfestival_backend.authentication.entity.Role;
import com.herokuapp.icecreamfestival_backend.authentication.entity.User;
import com.herokuapp.icecreamfestival_backend.authentication.service.UserService;
import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import com.herokuapp.icecreamfestival_backend.stock.repository.FoodStockRepository;
import com.herokuapp.icecreamfestival_backend.stock.service.FoodStockService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.security.Principal;

@SpringBootApplication
@RestController
public class IceCreamFestivalBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(IceCreamFestivalBackendApplication.class, args);
    }

    @RequestMapping(value = "/user")
    public Principal user(Principal principal) {
        System.gc();
        return principal;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:9000");
            }
        };
    }

    @Bean
    public CommandLineRunner setUpSeller(UserService userService) {
        if (userService.findByUsername("admin").isPresent()) {
            return args -> {
            };
        } else {
            return args -> {
                userService.save(new User(
                        "admin", //username
                        "admin@email.com", //email
                        "secret", //password
                        Arrays.asList(new Role("SELLER")),//roles
                        true//Active
                ));
            };
        }
    }

    @Bean
    public CommandLineRunner setUpStocks(FoodStockService foodStockService) {
        if (!foodStockService.findAll().isEmpty()) {
            return args -> {
            };
        } else {
            return args -> {
                foodStockService.intializeData();
            };
        }

    }

}