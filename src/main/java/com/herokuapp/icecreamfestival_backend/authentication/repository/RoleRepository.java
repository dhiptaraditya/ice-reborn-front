package com.herokuapp.icecreamfestival_backend.authentication.repository;

import com.herokuapp.icecreamfestival_backend.authentication.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
