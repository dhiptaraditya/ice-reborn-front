package com.herokuapp.icecreamfestival_backend.authentication.controller;

import com.herokuapp.icecreamfestival_backend.authentication.entity.Role;
import com.herokuapp.icecreamfestival_backend.authentication.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    public ResponseEntity<List<Role>> findAll() {
        return ResponseEntity.ok(roleService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Role role) {
        return ResponseEntity.ok(roleService.save(role));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Role> findById(@PathVariable Long id) {
        Optional<Role> role = roleService.findById(id);
        if (!role.isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(role.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Role> update(@PathVariable Long id, @Valid @RequestBody Role role) {
        if (!roleService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(roleService.save(role));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!roleService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        roleService.deleteById(id);

        return ResponseEntity.ok().build();
    }

}
