package com.herokuapp.icecreamfestival_backend;

import com.herokuapp.icecreamfestival_backend.authentication.entity.User;
import com.herokuapp.icecreamfestival_backend.authentication.repository.UserRepository;
import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;
import com.herokuapp.icecreamfestival_backend.stock.entity.base.Banana;
import com.herokuapp.icecreamfestival_backend.stock.entity.base.Chocolate;
import com.herokuapp.icecreamfestival_backend.stock.entity.base.SeaSalt;
import com.herokuapp.icecreamfestival_backend.stock.entity.topping.*;
import com.herokuapp.icecreamfestival_backend.stock.repository.FoodStockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class CheckoutController {
    @Autowired
    private FoodStockRepository foodStockRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/api/checkout", method = RequestMethod.POST)
    public ResponseEntity<?> checkout(@RequestBody Map<String, String> checkout_item) {
        Map<String, String> map = checkout_item;
        HashMap<String, Object> response = new HashMap<>();
        User user = null;
        int readyOfBase = 0;
        ArrayList<String> list_bahan = new ArrayList<String>();
        ArrayList<Integer> banyak_bahan = new ArrayList<Integer>();
        BaseCreamMachine base = null;
        final int START = 0;
        final int CHECK_MONEY = 1;
        final int YOU_CAN_BUY = 2;
        final int YOU_CANT_BUY = 3;
        int state = START;

        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getValue());
            System.out.println(entry.getKey());

            if(entry.getKey().equals("user")){
                Optional<User> opt_user= (Optional<User>) userRepository.findByUsername(entry.getValue());

                user = opt_user.get();

            }else
            if(entry.getKey().equals("base_layer")){
                base = new BaseCreamMachine(entry.getValue());
                base.checkReady();
                System.out.println("gah cok");
                readyOfBase = base.state;
            } else {
                list_bahan.add(entry.getKey());
                banyak_bahan.add(Integer.valueOf(entry.getValue()));
            }

        }
        System.out.println("tralala");
        System.out.println(readyOfBase);
        ToppingMachine toppingMachine = null;
        if (readyOfBase == 1) {
            toppingMachine = new ToppingMachine(list_bahan, banyak_bahan);
            toppingMachine.checkReady();
        }

        IceCream ice = null;
        if (toppingMachine.state == 1 && state == START) {
            ice = create_iceCream(base.base, list_bahan, banyak_bahan);
            int next_state = YOU_CAN_BUY;
            if (user.getBalance() < ice.cost()) {
                next_state = YOU_CANT_BUY;
            }
            state = next_state;
        } else {
            System.out.println("YOU CANT MAKE ICE CREAM");
        }
        if (state == YOU_CAN_BUY) {
            for (int i = 0; i < list_bahan.size(); i++) {
                FoodStock stock = foodStockRepository.findByName(list_bahan.get(i));
                stock.setStock(stock.getStock() - banyak_bahan.get(i));
                foodStockRepository.save(stock);
            }
            user.setBalance(user.getBalance() - ice.cost());
            userRepository.save(user);
            response.put("message", "YOUVE SUCCESFULLY BOUGHT ICE CREAM");
            response.put("icecream", ice.getDescription());
            response.put("cost", ice.cost());
            response.put("balance_left", user.getBalance());
            Optional<User> opt_user= (Optional<User>) userRepository.findByUsername("admin");
            User admin = opt_user.get();
            admin.setBalance(admin.getBalance() + ice.cost());
            userRepository.save(admin);
        } else response.put("message", "YOUVE FAILED BOUGHT ICE CREAM OF SEVERAL REASON");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);

    }

    public IceCream create_iceCream(String base, ArrayList<String> daftar_bahan, ArrayList<Integer> banyak_bahan) {
        IceCream iceCream;
        if (base.equals("Banana")) iceCream = new Banana();
        else if (base.equals("Chocolate")) iceCream = new Chocolate();
        else iceCream = new SeaSalt();
        for (int i = 0; i < daftar_bahan.size(); i++) {
            for (int j = 0; j < banyak_bahan.get(i); j++) {
                if (daftar_bahan.get(i).equals("ChocoChip")) {
                    iceCream = new Chocochip(iceCream);
                } else if (daftar_bahan.get(i).equals("HoneyStar")) {
                    iceCream = new HoneyStar(iceCream);
                } else if (daftar_bahan.get(i).equals("Marshmallow")) {
                    iceCream = new Marshmallow(iceCream);
                } else if (daftar_bahan.get(i).equals("Oreo")) {
                    iceCream = new Oreo(iceCream);
                } else {
                    iceCream = new Popcorn(iceCream);
                }
            }
        }
        return iceCream;
    }

    public class BaseCreamMachine {
        final static int START = 0;
        final static int READY = 1;
        final static int FAILED = 2;

        int state = START;
        String base;

        public BaseCreamMachine(String base) {
            this.base = base;
        }

        public void checkReady() {
            if (state == START) {
                int next_state = READY;
                if (foodStockRepository.findByName(base).getStock() == 0) {
                    next_state = FAILED;
                }
                this.state = next_state;
            } else if (state == READY) {
                System.out.println("You're ready to add topping, no need to add more base");
            }
        }
    }

    public class ToppingMachine {
        final static int START = 0;
        final static int ALL_READY = 1;
        final static int FAILED = 2;

        ArrayList<String> daftar_bahan;
        ArrayList<Integer> banyak_bahan;
        int base_check;
        int state = START;

        public ToppingMachine(ArrayList<String> daftar_bahan, ArrayList<Integer> banyak_bahan) {
            this.daftar_bahan = daftar_bahan;
            this.banyak_bahan = banyak_bahan;
        }

        public void checkReady() {
            System.out.println("pekok");
            if(this.state == START){
                int next_state = ALL_READY;
                for (int i = 0; i < daftar_bahan.size(); i++) {
                    FoodStock stock = foodStockRepository.findByName(daftar_bahan.get(i));
                    if (stock.getStock() < banyak_bahan.get(i)) {
                        next_state = FAILED;
                    }
                }
                this.state = next_state;
            } else if (base_check == 2) {
                System.out.println("You were failed to create base layer");
            } else {
                System.out.println("you are not in a phase to add toppping");
            }
        }
    }
}
