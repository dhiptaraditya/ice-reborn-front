package com.herokuapp.icecreamfestival_backend;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainmenuController {
    @GetMapping("/mainmenu")
    public String greeting(@RequestParam(name = "visitor", required = true)
                                       String name, Model model) {
        model.addAttribute("visitor", name);
        if (name.equals("seller")) {
            return "penjual-mainmenu";
        } else {
            return "pembeli-mainmenu";
        }
    }
}

