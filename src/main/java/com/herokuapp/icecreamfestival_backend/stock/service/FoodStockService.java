package com.herokuapp.icecreamfestival_backend.stock.service;

import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import com.herokuapp.icecreamfestival_backend.stock.repository.FoodStockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class FoodStockService {

    @Autowired
    private FoodStockRepository foodStockRepository;

    public HashMap<String, Object> intializeData() {
        String[] daftar_nama_stock = {"Banana" ,"Chocolate" ,"SeaSalt" ,"Chocochip" ,"HoneyStar" ,"Marshmallow" ,"Oreo" ,"Popcorn"};
        Double[] harga_barang = {2.5, 2.0, 3.0, 0.2, 0.5, 0.5, 0.4, 0.3};
        HashMap<String, Object> response = new HashMap<>();
        for (int i=0;i<daftar_nama_stock.length;i++){
            if(foodStockRepository.findByName(daftar_nama_stock[i]) == null){
                FoodStock stock = new FoodStock(daftar_nama_stock[i], harga_barang[i]);
                foodStockRepository.save(stock);
                response.put(stock.getName(), stock.getStock());
            }
        }
        return response;
    }

    public List<FoodStock> findAll() {
        return foodStockRepository.findAll();
    }

}

