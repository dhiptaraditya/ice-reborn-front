package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Oreo extends IceCream {
    IceCream iceCream;

    public Oreo(IceCream iceCream) {
        this.iceCream = iceCream;
        this.name = "Oreo";
    }

    @Override
    public String getDescription() {
        if(iceCream.getDescription().contains("with extra")==true){
            return iceCream.getDescription() + ", Oreo";
        }else return iceCream.getDescription() + " with extra, Oreo";
    }

    @Override
    public double cost() {
        return 0.4 + iceCream.cost();
    }

}
