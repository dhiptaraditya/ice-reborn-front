package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Chocochip extends IceCream {
    IceCream iceCream;

    public Chocochip(IceCream iceCream) {
        this.iceCream = iceCream;
        this.name = "Chocochip";
    }

    @Override
    public String getDescription() {
        if(iceCream.getDescription().contains("with extra")==true){
            return iceCream.getDescription() + ", Chocochip";
        }else return iceCream.getDescription() + " with extra, Chocochip";
    }

    @Override
    public double cost() {
        return 0.2 + iceCream.cost();
    }

}
