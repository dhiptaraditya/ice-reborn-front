package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public enum ToppingDecorator {
    CHOCOCHIP,
    HONEYSTAR,
    MARSHMALLOW,
    OREO,
    POPCORN;

    /**
     * decorate Base Ice Cream with a available Topping
     * if stock Topping not available return base;
     *
     * @param base that whant to be added
     * @return base that had been added
     */
    public IceCream addToppingToBaseIceCream(IceCream base) {
        IceCream baseDecorated;
        switch (this) {
            case CHOCOCHIP:
                baseDecorated = new Chocochip(base);
                break;
            case HONEYSTAR:
                baseDecorated = new HoneyStar(base);
                break;
            case MARSHMALLOW:
                baseDecorated = new Marshmallow(base);
                break;
            case OREO:
                baseDecorated = new Oreo(base);
                break;
            case POPCORN:
                baseDecorated = new Popcorn(base);
                break;

            default:
                baseDecorated = new Oreo(base);
                break;
        }
        if (baseDecorated.checkStock() < 1) {
            return base;
        }
        return baseDecorated;
    }
}
