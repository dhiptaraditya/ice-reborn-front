package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class HoneyStar extends IceCream {
    IceCream iceCream;

    public HoneyStar(IceCream iceCream) {
        this.iceCream = iceCream;
        this.name = "Honey Star";
    }

    @Override
    public String getDescription() {
        if(iceCream.getDescription().contains("with extra")==true){
            return iceCream.getDescription() + ", Honey Star";
        }else return iceCream.getDescription() + " with extra, Honey Star";
    }

    @Override
    public double cost() {
        return 0.5 + iceCream.cost();
    }

}
