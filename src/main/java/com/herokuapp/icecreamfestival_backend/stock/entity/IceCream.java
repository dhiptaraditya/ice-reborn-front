package com.herokuapp.icecreamfestival_backend.stock.entity;

import com.herokuapp.icecreamfestival_backend.stock.repository.FoodStockRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class IceCream {
    @Autowired
    private FoodStockRepository foodStockRepository;

    protected String name;

    protected String description = "Unidentified Food";

    public String getDescription() {
        return description;
    }

    public abstract double cost();

    public int checkStock() {
        return foodStockRepository.findByName(this.name).getStock();
    }
}

