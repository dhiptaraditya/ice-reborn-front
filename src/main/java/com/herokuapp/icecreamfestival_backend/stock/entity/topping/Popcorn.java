package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Popcorn extends IceCream {
    IceCream iceCream;

    public Popcorn(IceCream iceCream) {
        this.iceCream = iceCream;
        this.name = "Popcorn";
    }

    @Override
    public String getDescription() {
        if(iceCream.getDescription().contains("with extra")==true){
            return iceCream.getDescription() + ", Popcorn";
        }else return iceCream.getDescription() + " with extra, Popcorn";
    }

    @Override
    public double cost() {
        return 0.3 + iceCream.cost();
    }

}
