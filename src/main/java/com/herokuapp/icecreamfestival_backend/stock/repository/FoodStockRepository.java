package com.herokuapp.icecreamfestival_backend.stock.repository;

import com.herokuapp.icecreamfestival_backend.stock.entity.FoodStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodStockRepository extends JpaRepository<FoodStock, Integer>, JpaSpecificationExecutor<FoodStock> {

    @Query
    FoodStock findById(int id);

    @Query
    FoodStock findByName(String name);


}
