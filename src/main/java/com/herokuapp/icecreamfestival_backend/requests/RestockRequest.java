package com.herokuapp.icecreamfestival_backend.requests;

import com.fasterxml.jackson.annotation.JsonProperty;


public class RestockRequest {

    @JsonProperty("id")
    private int id;

    @JsonProperty("item")
    private String itemName;

    @JsonProperty("stok")
    private int stok;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }
}
